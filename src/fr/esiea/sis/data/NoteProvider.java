package fr.esiea.sis.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class NoteProvider extends ContentProvider {

    private Database mDatabase;

    public static final String AUTHORITY = "fr.esiea.sis.provider";
    public static final String TYPE_SINGLE_NOTE = "vnd.android.cursor.item/note";
    public static UriMatcher matcher;

    public static final String TYPE_ALL_NOTES = "vnd.android.cursor.dir/note";

    static {
        matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, "notes", 1);
        matcher.addURI(AUTHORITY, "note/#", 2);
    }

    public NoteProvider() {
    }

    @Override
    public boolean onCreate() {
        mDatabase = new Database(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        int match = matcher.match(uri);
        if(match == 2)
            return TYPE_SINGLE_NOTE;
        return TYPE_ALL_NOTES;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if(values == null || values.size() == 0)
            throw new IllegalArgumentException();

        SQLiteDatabase db = mDatabase.getWritableDatabase();
        long rowId = db.insertWithOnConflict("notes", null, values, SQLiteDatabase.CONFLICT_IGNORE);
        Uri newUri = ContentUris.withAppendedId(uri, rowId);
        return newUri;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mDatabase.getReadableDatabase();
        int match = matcher.match(uri);

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables("notes");

        if(match == 2){
            queryBuilder.appendWhereEscapeString("_id =" + ContentUris.parseId(uri));
        }

        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mDatabase.getWritableDatabase();
        int match = matcher.match(uri);
        int count;
        if(match == 2){
            count = db.delete("notes", "_id = "+String.valueOf(ContentUris.parseId(uri)), null);
        } else {
            count = db.delete("notes", null, null);
        }
        return count;
    }
}
