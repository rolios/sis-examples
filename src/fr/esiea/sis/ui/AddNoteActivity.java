package fr.esiea.sis.ui;

import java.util.Date;

import android.app.Activity;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.esiea.sis.R;
import fr.esiea.sis.data.NoteProvider;

public class AddNoteActivity extends Activity{

    private EditText mAuteur;
    private EditText mValeur;
    private Button mSaveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_note);

        mAuteur = (EditText) findViewById(R.id.auteur_edit);
        mValeur = (EditText) findViewById(R.id.valeur_edit);
        mSaveButton = (Button) findViewById(R.id.save_note_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }

    private void save() {
        String auteur = mAuteur.getText().toString();
        String valeur = mValeur.getText().toString();
        long date = new Date().getTime();

        Uri uri = Uri.parse("content://" + NoteProvider.AUTHORITY + "/notes");
        ContentValues values = new ContentValues(3);
        values.put("valeur", valeur);
        values.put("auteur", auteur);
        values.put("date", date);
        getContentResolver().insert(uri, values);

        finish();
    }
}
