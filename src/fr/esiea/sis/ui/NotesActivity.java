package fr.esiea.sis.ui;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;

import fr.esiea.sis.R;
import fr.esiea.sis.data.NoteProvider;

public class NotesActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor>{
    private static String TAG = NotesActivity.class.getSimpleName();

    private String[] mFrom;
    private int[] mTo;
    private SimpleCursorAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFrom = new String[]{"valeur","auteur"};
        mTo = new int[]{R.id.item_value, R.id.item_author};
        mAdapter = new SimpleCursorAdapter(this, R.layout.list_item, null, mFrom, mTo, 0);
        setListAdapter(mAdapter);
        getLoaderManager().initLoader(0, null, this);
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Uri uri = Uri.parse("content://" + NoteProvider.AUTHORITY + "/note");
                Cursor item = (Cursor) mAdapter.getItem(position);
                long columnId = item.getLong(item.getColumnIndex("_id"));
                uri = ContentUris.withAppendedId(uri, columnId);
                getContentResolver().delete(uri, null, null);
                return true;
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = Uri.parse("content://" + NoteProvider.AUTHORITY + "/notes");
        return new CursorLoader(this, uri, null, null, null,
        null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
